import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatButtonModule } from '@angular/material/button';
import { PipesModule } from '../pipes/pipes.module';
import {MatListModule} from '@angular/material/list';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import { OverlayModule } from '@angular/cdk/overlay';
import { DishComponent } from './dish/dish.component';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [

    ToolbarComponent,
     DishComponent,
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    PipesModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    OverlayModule
  ],
  exports:[
    ToolbarComponent,
    MatIconModule,
    MatMenuModule,
    DishComponent,
    
  ],
  providers:[OverlayModule]
})
export class ComponentsModule { }
