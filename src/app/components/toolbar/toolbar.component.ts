import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.interface';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: [ './toolbar.component.scss'
  ]
})
export class ToolbarComponent implements OnInit {

  public user!:User | null;
  constructor(private _authService:AuthService,private _router:Router) { }

  ngOnInit(): void {
  }

  ngAfterContentInit(): void {
    let storage =localStorage.getItem('credentials');
    if(storage) this.user = JSON.parse(storage);
     
  }

  logOut():void{
    this._authService.logOut();
    this._router.navigate(['/login']);
  }


  goHome():void{
    this._router.navigate(['/dish/home']);
  }

}
