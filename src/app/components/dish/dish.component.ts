import { Component, Input, OnInit } from '@angular/core';
import { Dish } from 'src/app/models/dishes.interface';

@Component({
  selector: 'app-dish-individual',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.scss'
  ]
})
export class DishComponent implements OnInit {

  @Input() dish!:Dish;
  constructor() { }

  ngOnInit(): void {
  }

}
