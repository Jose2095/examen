import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Dish, Ingredient } from 'src/app/models/dishes.interface';
import { environment } from 'src/environments/environment';

interface Response{
  meals:Array<Dish>
}
@Injectable({
  providedIn: 'root'
})
export class DishesService {

  private _url= environment.apiUrl;
  constructor(private _http:HttpClient) { }


  getRandomDish():Observable<Response>{
    return this._http.get<Response>(`${this._url}random.php`)
  }

  getIngredients():Observable<{meals:Array<Ingredient>}>{
    return this._http.get<{meals:Array<Ingredient>}>(`${this._url}list.php?i=list`);
  }

  getDishById(id:string):Observable<Response>{
    return this._http.get<Response>(`${this._url}lookup.php?i=${id}`)
  }

  getFilterDishes(name:string):Observable<Response>{
    return this._http.get<Response>(`${this._url}search.php?s=${name}`)
  }

  filterByMainIngredient(ingredient:string):Observable<Response>{
    return this._http.get<Response>(`${this._url}filter.php?i=${ingredient}`)
  }
}
 