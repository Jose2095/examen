import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from 'src/app/models/user.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService{

  private _user:BehaviorSubject<User | null>;
  constructor(private _http:HttpClient) {
    let storage = localStorage.getItem('credentials');
    if(!storage) {
      this._user = new BehaviorSubject<User | null>(null);
    }
    else{
      this._user= new BehaviorSubject<User | null>(JSON.parse(storage))
    }
   }


  login():Observable<User[]>{
    return this._http.get<User[]>('../../../assets/dummies/user.json');
  }

  logOut(){
    localStorage.removeItem('credentials')
    this._user.next(null);
  }

  public get user():User | null{
    return this._user.value;  }
}
