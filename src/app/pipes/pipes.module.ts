import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LimitArrayPipe } from './limit-array.pipe';
import { SafeUrlPipe } from './safe-url.pipe';



@NgModule({
  declarations: [LimitArrayPipe, SafeUrlPipe],
  imports: [
    CommonModule
  ],
  exports:[
    LimitArrayPipe,
    SafeUrlPipe
  ]

})
export class PipesModule { }
