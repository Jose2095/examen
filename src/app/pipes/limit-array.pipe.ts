import { Pipe, PipeTransform } from '@angular/core';
import { Dish, Ingredient } from '../models/dishes.interface';

@Pipe({
  name: 'limitArray'
})
export class LimitArrayPipe implements PipeTransform {

  transform(ingredients:Array<Ingredient | Dish>,limit:number): Array<any> {
    return ingredients?.slice(0,limit);
  }

}
