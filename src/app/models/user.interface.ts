
export interface User{
    name:string;
    lastName:string,
    email:string,
    user:string;
    password:string;
}