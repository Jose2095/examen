import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import {User} from 'src/app/models/user.interface';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'
  ]
})
export class LoginComponent implements OnInit {

  public loginForm:FormGroup;
 

  constructor(private _formBuilder:FormBuilder,private _authService:AuthService,private _router:Router) {
    let userRemember = localStorage.getItem('userRemember');
    this.loginForm= this._formBuilder.group({
      user:['',Validators.required],
      password:['',Validators.required],
      remember:[false]
    });  
    if(userRemember){
      this.loginForm.patchValue({user:userRemember,remember:true})
    }
   }

   ngOnInit(): void {
  }

   public login(){
     this._authService.login().subscribe(response=>{
      let credentials = this.loginForm.value;
      let user = response.find(user=>{
        return credentials.user == user.user && credentials.password == user.password
      });

      if(user){
        localStorage.setItem('credentials',JSON.stringify(user));
        this._router.navigate(['/dish/home']);

        if(credentials.remember){
         localStorage.setItem('userRemember',user.user)
        }
        else{
         localStorage.removeItem('userRemember');
        }
      }
      else{
        alert('Invalid user')
      }
  
     })
   }



}
