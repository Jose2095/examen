import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Dish } from 'src/app/models/dishes.interface';
import { DishesService } from 'src/app/services/dish/dishes.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'
  ]
})
export class DetailComponent implements OnInit {

  private _dishId!:string;
  public currentDish!:Dish;
  constructor(private _route:ActivatedRoute,private _dishService:DishesService) { }

  ngOnInit(): void {
    this._getParam();
    
  }


  private _getParam(): void{
  
      this._route.paramMap
        .subscribe((params:any) => {
          this._dishId = params.params.id;
          console.log(this._dishId);
          
          this._getDetail();
        }
      );
    
  }

 private _getDetail(): void{
    this._dishService
    .getDishById(this._dishId)
    .subscribe(response=>{
      this.currentDish= response.meals[0];
    })

  }
}
