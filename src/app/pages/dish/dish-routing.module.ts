import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DishesComponent } from 'src/app/pages/dish/dishes/dishes.component';
import { IngredientsComponent } from 'src/app/pages/dish/ingredients/ingredients.component';
import { DetailComponent } from './detail/detail.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
  path:'home',
  component: HomeComponent
},
{
  path:'detail/:id',
  component:DetailComponent
},
{
  path:'ingredients',
  component:IngredientsComponent
},
{
  path:'list',
  component:DishesComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DishRoutingModule { }
