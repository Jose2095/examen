import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { WelcomeComponent } from 'src/app/dialogs/welcome/welcome.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss' ]
})
export class HomeComponent implements OnInit {

  constructor(private _dialog:MatDialog) { }

  ngOnInit(): void {
    this._dialog.open(WelcomeComponent)
  }

}
