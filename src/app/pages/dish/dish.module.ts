import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DishRoutingModule } from './dish-routing.module';
import { HomeComponent } from './home/home.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { DialogsModule } from 'src/app/dialogs/dialogs.module';
import { DetailComponent } from './detail/detail.component';
import { PipesModule } from 'src/app/pipes/pipes.module';
import {MatIconModule} from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { DishComponent } from './dish.component';
import { MatListModule } from '@angular/material/list';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { DishesComponent } from './dishes/dishes.component';
import { MatCardModule } from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
@NgModule({
  declarations: [
    HomeComponent,
    DetailComponent,
    DishComponent,
    IngredientsComponent,
    DishesComponent,
  ],
  imports: [
    CommonModule,
    DishRoutingModule,ComponentsModule,
    DialogsModule,
    PipesModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatCardModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    ComponentsModule
  ]
})
export class DishModule { }
