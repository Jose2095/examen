import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Dish } from 'src/app/models/dishes.interface';
import { DishesService } from 'src/app/services/dish/dishes.service';


@Component({
  selector: 'app-dishes',
  templateUrl: './dishes.component.html',
  styleUrls: ['./dishes.component.scss'
  ]
})
export class DishesComponent implements OnInit {

  public dishes!:Array<Dish>;
  public search=new FormControl('');

  constructor(private _dishService:DishesService,private _router:Router){}

  ngOnInit(): void {
    this._getDishes('');
  }


  private _getDishes(param:string):void{
    this._dishService
    .getFilterDishes(param)
    .subscribe(response=>{
      this.dishes=response.meals;
    })
  }

  goDetail(id:string):void{
    this._router.navigate(['/dish/detail',id])
  }

  searchAction():void{
    this._getDishes(this.search.value);
  }
}
