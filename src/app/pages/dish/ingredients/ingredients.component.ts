import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Dish, Ingredient } from 'src/app/models/dishes.interface';
import { DishesService } from 'src/app/services/dish/dishes.service';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.scss'
  ]
})
export class IngredientsComponent implements OnInit {
  
  public ingredients!:Array<Ingredient>;
  public meals!:Array<Dish>;

  constructor(private _dishesService:DishesService,private _router:Router) { }

  ngOnInit(): void {
    this.getIngredients();
  }

  getIngredients(): void {
    this._dishesService
    .getIngredients()
    .subscribe(response=>{
      this.ingredients = response.meals;
    })
  }

  showDishes(ingredient:string):void{
    this._dishesService.filterByMainIngredient(ingredient)
    .subscribe(response=>{
      this.meals = response.meals;
    })
  }

  goDetail(id:string):void{
    this._router.navigate(['dish/detail',id])
  }
}
