import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Dish } from 'src/app/models/dishes.interface';
import { DishesService } from 'src/app/services/dish/dishes.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'
  ]
})
export class WelcomeComponent implements OnInit {

  public randomDish!:Dish;

  constructor(private _dishService:DishesService,private _router:Router,private _dialogRef:MatDialogRef<WelcomeComponent>) { }

  ngOnInit(): void {
    this.getRandomDish();
  }

  getRandomDish():void{
    this._dishService
    .getRandomDish()
    .subscribe(response=>{
      this.randomDish = response.meals[0];
    })
  }

  goDetail():void{
    this._router.navigate(['/dish/detail',this.randomDish.idMeal]);
    this._dialogRef.close();
  }
}
